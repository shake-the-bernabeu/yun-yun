package com.yunmai.web.repositories.good;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yunmai.web.domain.good.GoodEntity;



public interface IGoodMapper extends BaseMapper<GoodEntity> {
}
