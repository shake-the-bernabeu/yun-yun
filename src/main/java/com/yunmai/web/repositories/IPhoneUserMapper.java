package com.yunmai.web.repositories;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yunmai.web.domain.PhoneUserEntity;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author WangChen
 * @since 2021-03-17
 */
public interface IPhoneUserMapper extends BaseMapper<PhoneUserEntity> {

}
