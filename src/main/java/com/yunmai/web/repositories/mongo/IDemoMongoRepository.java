package com.yunmai.web.repositories.mongo;

import com.yunmai.web.domain.mongo.MongoDemoEntity;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface IDemoMongoRepository extends MongoRepository<MongoDemoEntity, String> {

    MongoDemoEntity getFirstByGuid(String guid);
}

