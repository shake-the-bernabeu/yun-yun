package com.yunmai.web.repositories.book;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yunmai.web.domain.book.BookEntity;

public interface IBookMapper extends BaseMapper<BookEntity> {
}
