package com.yunmai.web.repositories.student;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yunmai.web.domain.student.StudentEntity;

/**
 * <p>
 *  StudentMapper 接口
 * </p>
 *
 * @author WangChen
 * @since 2021-04-09
 */
public interface IStudentMapper extends BaseMapper<StudentEntity> {

}
