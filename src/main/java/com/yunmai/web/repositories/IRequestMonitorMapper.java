package com.yunmai.web.repositories;

import com.yunmai.web.domain.RequestMonitorEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author WangChen
 * @since 2021-04-08
 */
public interface IRequestMonitorMapper extends BaseMapper<RequestMonitorEntity> {

}
