package com.yunmai.web.repositories;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.yunmai.web.domain.AccountDetailEntity;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author WangChen
 * @since 2021-03-12
 */
public interface IAccountDetailMapper extends BaseMapper<AccountDetailEntity> {

}
