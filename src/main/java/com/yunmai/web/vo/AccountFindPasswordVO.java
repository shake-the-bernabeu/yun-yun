package com.yunmai.web.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author WangChen
 * @since 2021-01-07 09:19
 **/
@ApiModel
public class AccountFindPasswordVO {

    @Pattern(regexp = "(?:0|86|\\+86)?1[3-9]\\d{9}", message = "{validated.user.phone.not.right}")
    @JsonProperty(value = "auth_id")
    @ApiModelProperty(value = "手机号")
    private String phone;

    @NotBlank(message = "{validated.user.password.not.empty}")
    @ApiModelProperty(name = "password")
    @JsonProperty(value = "password")
    private String password;

    @NotBlank(message = "{validated.user.message.code.not.empty}")
    @JsonProperty(value = "code")
    @ApiModelProperty(value = "验证码")
    private String code;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "AccountFindPasswordDTO{" +
                "phone='" + phone + '\'' +
                ", password='" + password + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
