package com.yunmai.web.vo.book;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yunmai.web.vo.student.BaseReqDTO;
import io.swagger.annotations.ApiModel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.text.DecimalFormat;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
@ApiModel
public class BookUpdateReqDTO extends BaseReqDTO {
    @NotEmpty(message="姓名不能为空")
    private String name;

    @Min(value = 0,message = "价格必须大于0")
    private DecimalFormat price;

    @NotEmpty(message = "作者不能为空")
    private String authors;
    @NotEmpty(message = "作者不能为空")
    private String isbn;

    @NotEmpty(message = "作者不能为空")
    @JsonProperty(value = "publishing_house")
    private String publishingHouse;
    @NotNull(message = "数量不能为空")
    @Max(value = 100,message = "数量不能大于一百")
    private Integer number;

    @NotEmpty(message = "作者不能为空")
    @JsonProperty(value = "cover_image_url")
    private String coverImageUrl;
}
