package com.yunmai.web.vo.good;


import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel
public class GoodCreateReqDTO extends BaseReqDTO{






    @NotBlank(message = "商品名称不能为空")
    @JsonProperty(value = "name")
    @ApiModelProperty(value = "商品名称")
    private String name;

    @Min(value = 0, message = "最小为0")
    @Max(value = 10000, message = "最大为10000")
    @NotNull(message = "数量不能为空")
    @JsonProperty(value = "num")
    @ApiModelProperty(value = "数量")
    private Integer num;

    @NotBlank(message = "生产厂商不能为空")
    @JsonProperty(value = "factory")
    @ApiModelProperty(value = "生产厂商")
    private String factory;
}
