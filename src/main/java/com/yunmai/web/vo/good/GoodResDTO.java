package com.yunmai.web.vo.good;


import com.yunmai.framework.model.BaseDTO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@ApiModel
@NoArgsConstructor
public class GoodResDTO extends BaseDTO {

    @Override
    public String setModel() {
        return "good.model";
    }

    private String name;

    private Integer num;

    private String factory;
}
