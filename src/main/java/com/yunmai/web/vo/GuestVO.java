package com.yunmai.web.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;

/**
 * @author WangChen
 * @since 2021-01-12 13:55
 **/
@ApiModel
public class GuestVO {

    @NotBlank(message = "{validated.guest.clientId.not.empty.error.message}")
    @ApiModelProperty(name = "client_id")
    @JsonProperty("client_id")
    private String clientId;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public String toString() {
        return "GuestVO{" +
                "clientId='" + clientId + '\'' +
                '}';
    }
}
