package com.yunmai.web.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;

/**
 * @author WangChen
 * @since 2021-01-07 09:19
 **/
@ApiModel
public class AccountLoginSmsVO {

    @Pattern(regexp = "(?:0|86|\\+86)?1[3-9]\\d{9}", message = "{validated.user.phone.not.right}")
    @JsonProperty(value = "phone")
    @ApiModelProperty(value = "手机号")
    private String phone;

    @NotBlank(message = "{validated.user.message.code.not.empty}")
    @JsonProperty(value = "code")
    @ApiModelProperty(value = "验证码")
    private String code;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String toString() {
        return "AccountLoginSmsDTO{" +
                "phone='" + phone + '\'' +
                ", code='" + code + '\'' +
                '}';
    }
}
