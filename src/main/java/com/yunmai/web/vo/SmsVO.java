package com.yunmai.web.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import javax.validation.constraints.Pattern;

/**
 * @author WangChen
 * @since 2021-03-12 09:59
 **/
@ApiModel
public class SmsVO {

    @Pattern(regexp = "(?:0|86|\\+86)?1[3-9]\\d{9}", message = "{validated.user.phone.not.right}")
    @JsonProperty(value = "auth_id")
    @ApiModelProperty(value = "手机号")
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "SmsVO{" +
                "phone='" + phone + '\'' +
                '}';
    }
}
