package com.yunmai.web.vo.student;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yunmai.framework.model.BaseDTO;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;

/**
 * @author WangChen
 * @since 2021-04-09 09:16
 **/
@Data
@NoArgsConstructor
@AllArgsConstructor
@ToString
@ApiModel
public class StudentCreateReqDTO extends BaseReqDTO {
    /**
     * see jsr303规范
     */

    @NotBlank(message = "姓名不能为空")
    @JsonProperty(value = "name")
    @ApiModelProperty(value = "姓名")
    private String name;

    @Min(value = 1, message = "最小为1")
    @Max(value = 100, message = "最大为100")
    @NotNull(message = "年龄不能为空")
    @JsonProperty(value = "age")
    @ApiModelProperty(value = "年龄")
    private Integer age;

    @NotBlank(message = "地址不能为空")
    @JsonProperty(value = "address")
    @ApiModelProperty(value = "地址")
    private String address;

}
