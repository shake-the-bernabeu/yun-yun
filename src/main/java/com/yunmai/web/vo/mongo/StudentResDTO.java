package com.yunmai.web.vo.mongo;

import com.yunmai.framework.model.BaseDTO;
import io.swagger.annotations.ApiModel;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@ToString
@ApiModel
@NoArgsConstructor
public class StudentResDTO extends BaseDTO {

    @Override
    public String setModel() {
        return "student.model";
    }

    private String name;

    private Integer age;

    private String address;

}
