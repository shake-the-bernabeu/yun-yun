package com.yunmai.web.vo.redis;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;
import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KeyListDTO {
    @NotBlank(message = "Key不能为空")
    @JsonProperty(value = "key")
    @ApiModelProperty(value = "Key")
    private String key;

    @NotBlank(message = "ValueList不能为空")
    @JsonProperty(value = "valueList")
    @ApiModelProperty(value = "valueList")
    private List<String> valueList;
}
