package com.yunmai.web.vo.redis;

import com.fasterxml.jackson.annotation.JsonProperty;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class KeyValueDTO {
    @NotBlank(message = "Key不能为空")
    @JsonProperty(value = "key")
    @ApiModelProperty(value = "Key")
    private String key;

    @NotBlank(message = "Value不能为空")
    @JsonProperty(value = "value")
    @ApiModelProperty(value = "value")
    private String value;
}
