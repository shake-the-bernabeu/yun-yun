package com.yunmai.web.vo;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.yunmai.framework.model.BaseDTO;

import java.util.List;

/**
 * @author WangChen
 * @since 2021-04-07 17:02
 **/
public class AccountSessionInfo extends BaseDTO {

    @Override
    public String setModel() {
        return "account.session";
    }

    private String name;
    private String avatar;
    private String token;
    @JsonProperty("client_id")
    private String clientId;
    private List<String> powers;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public List<String> getPowers() {
        return powers;
    }

    public void setPowers(List<String> powers) {
        this.powers = powers;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    @Override
    public String toString() {
        return "AccountSessionInfo{" +
                "name='" + name + '\'' +
                ", avatar='" + avatar + '\'' +
                ", token='" + token + '\'' +
                ", clientId='" + clientId + '\'' +
                ", powers=" + powers +
                '}';
    }
}
