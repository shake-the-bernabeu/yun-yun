package com.yunmai.web.services;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yunmai.web.domain.good.GoodEntity;

public interface IGoodService  extends IService<GoodEntity> {
}
