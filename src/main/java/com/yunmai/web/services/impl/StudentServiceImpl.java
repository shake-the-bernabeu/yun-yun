package com.yunmai.web.services.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yunmai.web.domain.student.StudentEntity;
import com.yunmai.web.repositories.student.IStudentMapper;
import com.yunmai.web.services.IStudentService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WangChen
 * @since 2021-04-09
 */
@Service
public class StudentServiceImpl extends ServiceImpl<IStudentMapper, StudentEntity> implements IStudentService {

}
