package com.yunmai.web.services.impl;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yunmai.web.domain.AccountCenterUserEntity;
import com.yunmai.web.exception.BusinessResponseException;
import com.yunmai.web.exception.error.ErrorMessageResponse;
import com.yunmai.web.repositories.IAccountCenterUserMapper;
import com.yunmai.web.services.IAccountCenterUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WangChen
 * @since 2021-03-17
 */
@Service
public class AccountCenterUserServiceImpl extends ServiceImpl<IAccountCenterUserMapper, AccountCenterUserEntity> implements IAccountCenterUserService {

    private static final Logger logger  = LoggerFactory.getLogger(AccountCenterUserServiceImpl.class);

    @Autowired
    private IAccountCenterUserService accountCenterUserService;

    @Transactional(rollbackFor = Exception.class)
    @Override
    public AccountCenterUserEntity save(String authId, String password) {
        AccountCenterUserEntity accountCenterUserEntity = new AccountCenterUserEntity();
        accountCenterUserEntity.setAuthId(authId);
        accountCenterUserEntity.setPassword(password);
        accountCenterUserService.save(accountCenterUserEntity);
        return accountCenterUserEntity;
    }

    @Override
    public AccountCenterUserEntity find(String guid) {
        AccountCenterUserEntity accountCenterUserEntity = accountCenterUserService.getById(guid);
        if (ObjectUtil.isEmpty(accountCenterUserEntity)){
            logger.info("[AccountCenterUserServiceImpl] accountCenterUserEntity is null! guid = {}", guid);
            throw new BusinessResponseException(ErrorMessageResponse.ACCOUNT_NOT_EXIST.convert());
        }
        return accountCenterUserEntity;
    }
}
