package com.yunmai.web.services.impl;

import com.yunmai.web.domain.RequestMonitorEntity;
import com.yunmai.web.repositories.IRequestMonitorMapper;
import com.yunmai.web.services.IRequestMonitorService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WangChen
 * @since 2021-04-08
 */
@Service
public class RequestMonitorServiceImpl extends ServiceImpl<IRequestMonitorMapper, RequestMonitorEntity> implements IRequestMonitorService {

    @Autowired
    private IRequestMonitorService requestMonitorService;

    @Override
    public void saveTimeOutRequest(String path, Long millisecond) {
        RequestMonitorEntity requestMonitorEntity = new RequestMonitorEntity();
        requestMonitorEntity.setRequestPath(path);
        requestMonitorEntity.setMillisecond(millisecond);
        requestMonitorService.save(requestMonitorEntity);
    }
}
