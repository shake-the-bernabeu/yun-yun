package com.yunmai.web.services.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yunmai.web.domain.good.GoodEntity;
import com.yunmai.web.domain.student.StudentEntity;
import com.yunmai.web.repositories.good.IGoodMapper;
import com.yunmai.web.repositories.student.IStudentMapper;
import com.yunmai.web.services.IGoodService;
import com.yunmai.web.services.IStudentService;
import org.springframework.stereotype.Service;

@Service
public class GoodServiceImpl extends ServiceImpl<IGoodMapper, GoodEntity> implements IGoodService {
}
