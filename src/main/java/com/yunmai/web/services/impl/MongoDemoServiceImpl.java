package com.yunmai.web.services.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.yunmai.web.domain.mongo.MongoDemoEntity;
import com.yunmai.web.domain.student.StudentEntity;
import com.yunmai.web.repositories.mongo.IDemoMongoRepository;
import com.yunmai.web.repositories.student.IStudentMapper;
import com.yunmai.web.services.IMongoDemoService;
import com.yunmai.web.services.IStudentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author WangChen
 * @since 2021-04-09
 */
@Service
public class MongoDemoServiceImpl implements IMongoDemoService {

    @Autowired
    IDemoMongoRepository demoMongoRepository;

    @Override
    public void create(MongoDemoEntity mongoDemoEntity) {
        demoMongoRepository.save(mongoDemoEntity);
    }

    @Override
    public MongoDemoEntity getById(String guid) {
        MongoDemoEntity mongoDemoEntity = demoMongoRepository.getFirstByGuid(guid);

        return mongoDemoEntity;
    }
}
