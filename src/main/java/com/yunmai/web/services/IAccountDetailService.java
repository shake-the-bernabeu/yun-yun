package com.yunmai.web.services;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yunmai.web.domain.AccountDetailEntity;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author WangChen
 * @since 2021-03-12
 */
public interface IAccountDetailService extends IService<AccountDetailEntity> {

    /**
     * 创建详情
     *
     * @param authId
     * @param accountId
     */
    AccountDetailEntity create(String authId, String accountId);


    /**
     * 查找用户详情
     *
     * @param accountId
     * @return DeveloperUserDetailEntity
     */
    AccountDetailEntity find(String accountId);
}
