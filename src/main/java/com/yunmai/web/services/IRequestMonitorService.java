package com.yunmai.web.services;

import com.yunmai.web.domain.RequestMonitorEntity;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author WangChen
 * @since 2021-04-08
 */
public interface IRequestMonitorService extends IService<RequestMonitorEntity> {

    /**
     * 保存超时请求
     * @param path
     * @param millisecond
     */
    void saveTimeOutRequest(String path, Long millisecond);

}
