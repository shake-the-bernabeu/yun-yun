package com.yunmai.web.services;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yunmai.web.domain.AccountCenterUserEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author WangChen
 * @since 2021-03-17
 */
public interface IAccountCenterUserService extends IService<AccountCenterUserEntity> {

    /**
     * 保存用户中心账户密码
     * @param authId
     * @param password
     * @return AccountCenterUserEntity
     */
    AccountCenterUserEntity save(String authId, String password);


    /**
     * 查询用户中心账户密码
     * @param guid
     * @return AccountCenterUserEntity
     */
    AccountCenterUserEntity find(String guid);

}
