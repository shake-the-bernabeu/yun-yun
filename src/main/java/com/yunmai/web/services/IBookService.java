package com.yunmai.web.services;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yunmai.web.domain.book.BookEntity;

public interface IBookService extends IService<BookEntity> {
}
