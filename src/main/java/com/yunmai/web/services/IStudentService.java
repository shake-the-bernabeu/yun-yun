package com.yunmai.web.services;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yunmai.web.domain.student.StudentEntity;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author WangChen
 * @since 2021-04-09
 */
public interface IStudentService extends IService<StudentEntity> {

}
