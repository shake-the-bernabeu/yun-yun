package com.yunmai.web.services;

import com.yunmai.web.domain.mongo.MongoDemoEntity;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  Mongo服务接口
 * </p>
 *
 * @author WangChen
 * @since 2021-04-09
 */
public interface IMongoDemoService {
    void create(MongoDemoEntity mongoDemoEntity);
    MongoDemoEntity getById(String id);
}
