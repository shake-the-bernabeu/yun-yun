package com.yunmai.web.services;

import com.baomidou.mybatisplus.extension.service.IService;
import com.yunmai.web.domain.PhoneUserEntity;
import com.yunmai.web.vo.AccountLoginVO;
import com.yunmai.web.vo.AccountSessionInfo;
import com.yunmai.web.vo.AccountVO;

/**
 * <p>
 * 服务类
 * </p>
 *
 * @author WangChen
 * @since 2021-03-17
 */
public interface IPhoneUserService extends IService<PhoneUserEntity> {

    /**
     * 保存用户
     *
     * @param accountVO account
     */
    AccountSessionInfo register(AccountVO accountVO);


    /**
     * 保存guest用户
     */
    AccountSessionInfo registerGuest();


    /**
     * guest登录
     * @param clientId
     */
    AccountSessionInfo loginGuest(String clientId);


    /**
     * 登录
     *
     * @param accountLoginVO
     */
    AccountSessionInfo login(AccountLoginVO accountLoginVO);


    /**
     * 登录
     *
     * @param phone 手机号
     */
    AccountSessionInfo login(String phone);


    /**
     * 查找用户
     *
     * @param phone
     * @param password
     * @return PhoneUserEntity
     */
    PhoneUserEntity find(String phone, String password);


    /**
     * 查找用户
     *
     * @param phone
     * @return PhoneUserEntity
     */
    PhoneUserEntity find(String phone);


    /**
     * 修改密码
     *
     * @param phone
     * @param password
     * @return PhoneUserEntity
     */
    AccountSessionInfo changePassword(String phone, String password);


    /**
     * 获取session
     *
     * @return PhoneUserEntity
     */
    AccountSessionInfo getSession();

}
