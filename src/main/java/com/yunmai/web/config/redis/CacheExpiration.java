package com.yunmai.web.config.redis;

/**
 * 缓存实体
 * @author wangchen
 */
public enum CacheExpiration {

    /**
     * 用户详情
     */
    cache_account_detail(DurationConstant.ONE_WEEK);


    private long expire;

    CacheExpiration(long expire) {
        this.expire = expire;
    }

    public long getExpire() {
        return expire;
    }




}
