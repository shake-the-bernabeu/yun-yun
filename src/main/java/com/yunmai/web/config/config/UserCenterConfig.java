package com.yunmai.web.config.config;

import com.yunmai.account.center.sdk.api.RuntimeEnvironment;
import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author WangChen
 * @since 2021-04-06 14:13
 **/
@ConfigurationProperties(prefix = "user-center")
public class UserCenterConfig {

    private RuntimeEnvironment environment;


    public RuntimeEnvironment getEnvironment() {
        return environment;
    }

    public void setEnvironment(RuntimeEnvironment environment) {
        this.environment = environment;
    }
}
