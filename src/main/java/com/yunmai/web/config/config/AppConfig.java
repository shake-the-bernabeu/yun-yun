package com.yunmai.web.config.config;

import org.springframework.boot.context.properties.ConfigurationProperties;

/**
 * @author WangChen
 * @since 2021-01-19 11:36
 **/
@ConfigurationProperties(prefix = "app")
public class AppConfig {

    private String appId;
    private String secret;

    public String getAppId() {
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getSecret() {
        return secret;
    }

    public void setSecret(String secret) {
        this.secret = secret;
    }

    @Override
    public String toString() {
        return "AppConfig{" +
                "appId='" + appId + '\'' +
                ", secret='" + secret + '\'' +
                '}';
    }
}
