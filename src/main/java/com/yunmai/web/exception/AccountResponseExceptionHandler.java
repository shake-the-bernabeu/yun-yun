package com.yunmai.web.exception;

import cn.hutool.core.util.ObjectUtil;
import com.yunmai.account.center.sdk.exception.AccountCenterResponseException;
import com.yunmai.core.common.model.model.error.ErrorMessage;
import com.yunmai.core.common.model.model.error.GlobalErrorModel;
import com.yunmai.framework.exception.handler.ExceptionHandler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author WangChen
 * @since 2021-02-21 10:44
 **/
@Component
public class AccountResponseExceptionHandler implements ExceptionHandler<AccountCenterResponseException> {

    @Override
    public ResponseEntity<GlobalErrorModel> handle(AccountCenterResponseException exception) {
        List<ErrorMessage> errorMessage = exception.getErrorMessage();
        HttpStatus httpStatus = exception.getHttpStatus();
        //这只能单独处理options请求
        if (ObjectUtil.isEmpty(errorMessage)){
            return new ResponseEntity<>(httpStatus);
        }
        GlobalErrorModel globalErrorModel = GlobalErrorModel.of(errorMessage);
        return new ResponseEntity<>(globalErrorModel, httpStatus);
    }

}
