package com.yunmai.web.exception.error;

import com.yunmai.core.common.model.model.error.ErrorMessage;
import com.yunmai.framework.common.converter.Converter;
import com.yunmai.web.config.i18n.MultiLanguageUtils;

/**
 * @author WangChen
 */
public enum ErrorMessageResponse implements Converter<ErrorMessageResponse, ErrorMessage> {

    /**
     * token过期
     */
    TOKEN_EXPIRED("auth.error.code", "api.response.code.token.expired"),

    /**
     * 发送验证码失败
     */
    SEND_SMS_FAIL("auth.error.code","api.response.code.send.sms.fail"),

    /**
     * 验证码不正确
     */
    VERIFY_SMS_FAIL("auth.error.code","api.response.code.sms.code.not.right"),

    /**
     * 密码不正确
     */
    ACCOUNT_PWD_NOT_RIGHT("auth.error.code","api.response.code.account.pwd.not.right"),

    /**
     * 账号不存在
     */
    ACCOUNT_NOT_EXIST("auth.error.code","api.response.code.account.not.exist"),

    /**
     * 账号已经存在
     */
    ACCOUNT_ALREADY_EXIST("auth.error.code","api.response.code.account.exist");

    private String code;
    private String message;

    ErrorMessageResponse(String code, String message) {
        this.code = code;
        this.message = message;
    }

    public String getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public ErrorMessage convert(){
        return transform(this);
    }

    @Override
    public ErrorMessage transform(ErrorMessageResponse errorMessageResponse) {
        return new ErrorMessage(
                MultiLanguageUtils.getMessage(errorMessageResponse.code),
                MultiLanguageUtils.getMessage(errorMessageResponse.message)
        );
    }
}
