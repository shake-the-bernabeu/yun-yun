package com.yunmai.web.exception;

import com.yunmai.core.common.model.model.error.ErrorMessage;
import org.springframework.http.HttpStatus;


public class BusinessResponseException extends RuntimeException {

    private HttpStatus httpStatus;

    private ErrorMessage errorMessage;


    public BusinessResponseException(ErrorMessage errorMessage, HttpStatus httpStatus) {
        this.errorMessage = errorMessage;
        this.httpStatus = httpStatus;
    }

    public BusinessResponseException(ErrorMessage errorMessage) {
        this.errorMessage = errorMessage;
        this.httpStatus = HttpStatus.BAD_REQUEST;
    }


    public ErrorMessage getErrorMessage() {
        return errorMessage;
    }


    public HttpStatus getHttpStatus() {
        return httpStatus;
    }


}



