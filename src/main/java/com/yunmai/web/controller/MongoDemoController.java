package com.yunmai.web.controller;

import com.yunmai.core.common.model.model.OperationResult;
import com.yunmai.framework.utils.beancopy.BeanCopyUtils;
import com.yunmai.web.domain.mongo.MongoDemoEntity;
import com.yunmai.web.services.IMongoDemoService;
import com.yunmai.web.vo.mongo.StudentCreateReqDTO;
import com.yunmai.web.vo.mongo.StudentResDTO;
import com.yunmai.web.vo.redis.KeyListDTO;
import com.yunmai.web.vo.redis.KeyValueDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.*;

import java.util.Date;
import java.util.Set;
import java.util.UUID;

/**
 * @author WangChen
 * @since 2021-04-09 09:10
 **/
@Api(tags = "Mongo示例")
@RestController
@RequestMapping("/v1")
public class MongoDemoController {

    /**
     * @Validated
     * @Valid
     * 看jsr303规范
     */

    @Autowired
    IMongoDemoService mongoDemoService;


    @ApiOperation(
            value = "测试Redis写值",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
    })
    @PostMapping(value = "/demo/mongodb/student")
    public OperationResult<?> mongoCreate(
            @RequestBody StudentCreateReqDTO studentCreateReqDTO) {

        MongoDemoEntity mongoDemoEntity = BeanCopyUtils.copyProperties(studentCreateReqDTO,MongoDemoEntity::new);

        mongoDemoEntity.setId("stud_id_" + UUID.randomUUID().toString().replace("-",""));
        mongoDemoEntity.setCreated_time(new Date());
        mongoDemoEntity.setUpdated_time(new Date());
        mongoDemoEntity.setSort(0L);
        mongoDemoEntity.setVersion(0L);
        mongoDemoEntity.setDel(false);
        mongoDemoService.create(mongoDemoEntity);

        return OperationResult.ofSuccess(null);
    }

    @ApiOperation(
            value = "测试Redis读取值",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
    })
    @GetMapping(value = "/demo/mongodb/student/{stu_id}")
    public OperationResult<?> mongoRead(
            @PathVariable("stu_id") String stu_id) {

        MongoDemoEntity mongoDemoEntity = mongoDemoService.getById(stu_id);
        return OperationResult.ofSuccess(BeanCopyUtils.copyProperties(mongoDemoEntity, StudentResDTO::new));
    }

}
