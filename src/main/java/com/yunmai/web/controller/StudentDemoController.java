package com.yunmai.web.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yunmai.core.common.model.model.OperationResult;
import com.yunmai.framework.model.GridResult;
import com.yunmai.framework.spring.boot.annotation.EnableYmFrameworkAutoConfiguration;
import com.yunmai.framework.utils.beancopy.BeanCopyUtils;
import com.yunmai.web.domain.student.StudentEntity;
import com.yunmai.web.repositories.student.IStudentMapper;
import com.yunmai.web.services.IStudentService;
import com.yunmai.web.vo.redis.KeyListDTO;
import com.yunmai.web.vo.redis.KeyValueDTO;
import com.yunmai.web.vo.student.StudentCreateReqDTO;
import com.yunmai.web.vo.student.StudentResDTO;
import com.yunmai.web.vo.student.StudentUpdateReqDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * @author WangChen
 * @since 2021-04-09 09:10
 **/
@Api(tags = "学生增删改查实例")
@RestController
@RequestMapping("/v1")
public class StudentDemoController {

    /**
     * @Validated
     * @Valid
     * 看jsr303规范
     */

    @Autowired
    private IStudentService studentService;


    @ApiOperation(
            value = "创建学生记录",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class

    )
    @ApiImplicitParams({
            //@ApiImplicitParam(paramType = "header", dataType = "String", name = "X-AUTH-TOKEN", value = "token", required = true)
    })
    @PostMapping(value = "/demo/student")
    public OperationResult<?> create(@RequestBody @Validated StudentCreateReqDTO studentCreateReqDto) {
        StudentEntity studentEntity= BeanCopyUtils.copyProperties(studentCreateReqDto, StudentEntity::new);
        studentService.save(studentEntity);
        return OperationResult.ofSuccess(BeanCopyUtils.copyProperties(studentEntity,StudentResDTO::new));
    }



    @ApiOperation(
            value = "读取学生列表",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "search", value = "查询字段", required = false,dataType = "String"),
            @ApiImplicitParam(name = "size", value = "每页记录条数", required = true, dataTypeClass = int.class),
            @ApiImplicitParam(name = "page", value = "列表页码", required = true, dataTypeClass = int.class),
            @ApiImplicitParam(name = "sort", value = "排序字段", required = true, dataType = "String")
    })
    @GetMapping(value = "/demo/student")
    public GridResult<?> read(@RequestParam(value = "search",required = false) String search,
                              @RequestParam(value = "size",defaultValue = "10") int size,
                              @RequestParam(value = "page",defaultValue = "1") int page,
                              @RequestParam(value = "sort",defaultValue = "created_time") String sort) {

        //search , sort等定义传模式 可以是json都可以 看需求
        LambdaQueryWrapper<StudentEntity> queryWrapper = Wrappers.lambdaQuery(StudentEntity.class)
                //.eq(EgEntity::getAge, 10)
                .like(StudentEntity::getName,"%"+(search==null?"":search.trim())+"%")
                //.eq(EgEntity::getName, "123")
                .orderByDesc(StudentEntity::getCreatedTime);



        return GridResult.of(studentService.page(new Page<>(page, size), queryWrapper),StudentResDTO::new);
    }


    @ApiOperation(
            value = "读取学生列表",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "stu_id", value = "学生编号", required = true,dataType = "String",paramType = "path")
    })
    @GetMapping(value = "/demo/student/{stu_id}")
    public OperationResult<?> read(@PathVariable(value = "stu_id") String stu_id) {
        return OperationResult.ofSuccess(BeanCopyUtils.copyProperties(studentService.getById(stu_id),StudentResDTO::new));
    }

    @ApiOperation(
            value = "更新学生记录",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
            //@ApiImplicitParam(paramType = "header", dataType = "String", name = "X-AUTH-TOKEN", value = "token", required = true)
    })
    @PutMapping(value = "/demo/student/{stu_id}")
    public OperationResult<?> update(@PathVariable("stu_id") String stu_id,
                                     @RequestBody @Validated StudentUpdateReqDTO studentUpdateReqDto) {

        StudentEntity studentEntity = studentService.getById(stu_id);

        if (ObjectUtil.isEmpty(studentEntity)){
            return OperationResult.ofSuccess(null);
        }

        studentEntity.setName(studentUpdateReqDto.getName());
        studentEntity.setAddress(studentUpdateReqDto.getAddress());
        studentEntity.setAge(studentUpdateReqDto.getAge());

        studentService.updateById(studentEntity);

        return OperationResult.ofSuccess(BeanCopyUtils.copyProperties(studentEntity,StudentResDTO::new));
    }


    @ApiOperation(
            value = "删除学生记录",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
            //@ApiImplicitParam(paramType = "header", dataType = "String", name = "X-AUTH-TOKEN", value = "token", required = true)
    })
    @DeleteMapping(value = "/demo/student/{stu_id}")
    public OperationResult<?> delete(@PathVariable("stu_id") String stu_id) {
        StudentEntity studentEntity = studentService.getById(stu_id);
        studentService.removeById(stu_id);
        return OperationResult.ofSuccess(BeanCopyUtils.copyProperties(studentEntity, StudentResDTO::new));
    }
}
