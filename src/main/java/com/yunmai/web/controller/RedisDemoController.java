package com.yunmai.web.controller;

import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yunmai.core.common.model.model.OperationResult;
import com.yunmai.framework.model.GridResult;
import com.yunmai.framework.utils.beancopy.BeanCopyUtils;
import com.yunmai.web.domain.student.StudentEntity;
import com.yunmai.web.services.IStudentService;
import com.yunmai.web.vo.redis.KeyListDTO;
import com.yunmai.web.vo.redis.KeyValueDTO;
import com.yunmai.web.vo.student.StudentCreateReqDTO;
import com.yunmai.web.vo.student.StudentResDTO;
import com.yunmai.web.vo.student.StudentUpdateReqDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.Set;

/**
 * @author WangChen
 * @since 2021-04-09 09:10
 **/
@Api(tags = "Redis示例")
@RestController
@RequestMapping("/v1")
public class RedisDemoController {

    /**
     * @Validated
     * @Valid
     * 看jsr303规范
     */

    @Autowired
    RedisTemplate<String,Object> redisTemplate;


    @ApiOperation(
            value = "测试Redis写值",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
    })
    @PostMapping(value = "/demo/redis/value")
    public OperationResult<?> redisKeyValueWrite(
            @RequestBody KeyValueDTO keyValueDTO) {

        redisTemplate.opsForValue().set(keyValueDTO.getKey(),keyValueDTO.getValue());
        return OperationResult.ofSuccess(null);
    }

    @ApiOperation(
            value = "测试Redis读取值",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
    })
    @GetMapping(value = "/demo/redis/value/{key}")
    public OperationResult<?> redisKeyValueRead(
            @PathVariable("key") String key) {

        Object obj = redisTemplate.opsForValue().get(key);
        return OperationResult.ofSuccess(obj);
    }

    @ApiOperation(
            value = "测试Redis写集合",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
    })
    @PostMapping(value = "/demo/redis/list")
    public OperationResult<?> redisKeyListWrite(
            @RequestBody KeyListDTO keyListDTO) {

        redisTemplate.opsForSet().add(keyListDTO.getKey(),keyListDTO.getValueList());

        return OperationResult.ofSuccess(null);
    }

    @ApiOperation(
            value = "测试Redis读取集合",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
    })
    @GetMapping(value = "/demo/redis/list/{key}")
    public OperationResult<?> redisKeyListRead(
            @PathVariable("key") String key) {

        Set<Object> listValues = redisTemplate.opsForSet().members(key);

        return OperationResult.ofSuccess(listValues);
    }
}
