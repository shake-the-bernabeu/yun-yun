package com.yunmai.web.controller;

import com.yunmai.account.center.sdk.api.UserCenterApi;
import com.yunmai.core.common.model.model.OperationResult;
import com.yunmai.web.common.Constant;
import com.yunmai.web.common.sms.SmsCacheService;
import com.yunmai.web.services.IPhoneUserService;
import com.yunmai.web.vo.*;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletRequest;
import java.util.concurrent.TimeUnit;

/**
 * @author WangChen
 * @since 2021-04-06 13:47
 **/
@Api(tags = "用户")
@RestController
@RequestMapping("/v1")
public class UserCenterController {

    @Autowired
    private SmsCacheService smsCacheService;

    @Autowired
    private IPhoneUserService phoneUserService;


    @ApiOperation(
            value = "注册用户",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
    })
    @PostMapping("/accounts")
    public OperationResult<?> register(@RequestBody @Validated AccountVO accountVO) {

        smsCacheService.verify(accountVO.getPhone(), accountVO.getCode());

        return OperationResult.ofSuccess(phoneUserService.register(accountVO));
    }


    @ApiOperation(
            value = "登录(账号密码)",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
    })
    @PostMapping("/auth/accounts")
    public OperationResult<?> login(@RequestBody @Validated AccountLoginVO accountLoginVO) {

        return OperationResult.ofSuccess(phoneUserService.login(accountLoginVO));
    }


    @ApiOperation(
            value = "登录(短信验证)",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
    })
    @PostMapping("/auth/accounts/phone")
    public OperationResult<?> loginSms(@RequestBody @Validated AccountLoginSmsVO accountLoginVO) {

        smsCacheService.verify(accountLoginVO.getPhone(), accountLoginVO.getCode());

        return OperationResult.ofSuccess(phoneUserService.login(accountLoginVO.getPhone()));

    }


    @ApiOperation(
            value = "找回密码",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
    })
    @PostMapping("/auth/accounts/password")
    public OperationResult<?> findPassword(@RequestBody @Validated AccountFindPasswordVO accountFindPasswordVO) {

        smsCacheService.verify(accountFindPasswordVO.getPhone(), accountFindPasswordVO.getCode());

        return OperationResult.ofSuccess(phoneUserService.changePassword(accountFindPasswordVO.getPhone(), accountFindPasswordVO.getPassword()));
    }


    @ApiOperation(
            value = "注册guest",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
    })
    @PostMapping("/guests")
    public OperationResult<?> guestRegister() {
        return OperationResult.ofSuccess(phoneUserService.registerGuest());
    }


    @ApiOperation(
            value = "guest登录",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
    })
    @PostMapping("/auth/guests")
    public OperationResult<?> guestLogin(@RequestBody @Validated GuestVO guestVO) {
        return OperationResult.ofSuccess(phoneUserService.loginGuest(guestVO.getClientId()));

    }


    @ApiOperation(
            value = "发送短信验证码",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
    })
    @PostMapping("/code/sms")
    public OperationResult<?> sendSms(@RequestBody @Validated SmsVO smsVO) {
        return OperationResult.ofSuccess(smsCacheService.send(smsVO.getPhone()));
    }


    @ApiOperation(
            value = "验证token有效性",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "header", dataType = "String", name = "X-AUTH-TOKEN", value = "token", required = true)
    })
    @RequestMapping(value = "/sessions", method = RequestMethod.OPTIONS)
    public void optionsAuthToken(HttpServletRequest httpServletRequest) {
        UserCenterApi.user().verifySession(httpServletRequest.getHeader(Constant.TOKEN));
    }


    @ApiOperation(
            value = "获取session信息",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(paramType = "header", dataType = "String", name = "X-AUTH-TOKEN", value = "token", required = true)
    })
    @GetMapping(value = "/sessions")
    public OperationResult<?> optionsAuthToken() {
        return OperationResult.ofSuccess(phoneUserService.getSession());
    }
}
