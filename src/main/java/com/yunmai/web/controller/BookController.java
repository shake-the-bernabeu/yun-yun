package com.yunmai.web.controller;

import cn.hutool.core.util.ObjectUtil;
import com.yunmai.core.common.model.model.OperationResult;
import com.yunmai.framework.utils.beancopy.BeanCopyUtils;
import com.yunmai.web.domain.book.BookEntity;
import com.yunmai.web.domain.student.StudentEntity;
import com.yunmai.web.services.IBookService;
import com.yunmai.web.vo.book.BookCreateReqDTO;
import com.yunmai.web.vo.book.BookResDTO;
import com.yunmai.web.vo.book.BookUpdateReqDTO;
import com.yunmai.web.vo.student.StudentResDTO;
import com.yunmai.web.vo.student.StudentUpdateReqDTO;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("v1")
public class BookController {
    @Autowired
    private IBookService bookService;

    @PostMapping("/creat")
    public OperationResult<?> creat(@RequestBody @Validated BookCreateReqDTO bookCreateReqDTO){
        BookEntity bookEntity = BeanCopyUtils.copyProperties(bookCreateReqDTO, BookEntity::new);
        bookService.save(bookEntity);
        return OperationResult.ofSuccess(BeanCopyUtils.copyProperties(bookEntity, BookResDTO::new));
    }

    @PutMapping(value = "/demo/book/{id}")
    public OperationResult<?> update(@PathVariable("id") String id,
                                     @RequestBody @Validated BookUpdateReqDTO bookUpdateReqDTO) {

        BookEntity bookEntity = bookService.getById(id);

        if (ObjectUtil.isEmpty(bookEntity)){
            return OperationResult.ofSuccess(null);
        }
        bookService.updateById(bookEntity);

        return OperationResult.ofSuccess(BeanCopyUtils.copyProperties(bookEntity, StudentResDTO::new));
    }


}
