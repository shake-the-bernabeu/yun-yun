package com.yunmai.web.controller;


import cn.hutool.core.util.ObjectUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.yunmai.core.common.model.model.OperationResult;
import com.yunmai.framework.model.GridResult;
import com.yunmai.framework.utils.beancopy.BeanCopyUtils;
import com.yunmai.web.domain.good.GoodEntity;
import com.yunmai.web.domain.student.StudentEntity;
import com.yunmai.web.services.IGoodService;
import com.yunmai.web.vo.good.GoodCreateReqDTO;
import com.yunmai.web.vo.good.GoodResDTO;
import com.yunmai.web.vo.good.GoodUpdateRedDTO;
import com.yunmai.web.vo.student.StudentCreateReqDTO;
import com.yunmai.web.vo.student.StudentResDTO;
import com.yunmai.web.vo.student.StudentUpdateReqDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Api(tags = "商品增删改查实例")
@RestController
@RequestMapping("/v1")
public class GoodDemoController {


    @Autowired
    private IGoodService    goodService;




    //增加
    @ApiOperation(
            value = "创建商品记录",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
    })
    @PostMapping(value = "/demo/good")
    public OperationResult<?> create(@RequestBody @Validated GoodCreateReqDTO goodCreateReqDto) {
        GoodEntity goodEntity= BeanCopyUtils.copyProperties(goodCreateReqDto, GoodEntity::new);
        goodService.save(goodEntity);
        return OperationResult.ofSuccess(BeanCopyUtils.copyProperties(goodEntity, GoodResDTO::new));
    }





  //查询所有
    @ApiOperation(
            value = "读取商品列表",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "search", value = "查询字段", required = false,dataType = "String"),
            @ApiImplicitParam(name = "size", value = "每页记录条数", required = true, dataTypeClass = int.class),
            @ApiImplicitParam(name = "page", value = "列表页码", required = true, dataTypeClass = int.class),
            @ApiImplicitParam(name = "sort", value = "排序字段", required = true, dataType = "String")
    })
    @GetMapping(value = "/demo/good")
    public GridResult<?> read(@RequestParam(value = "search",required = false) String search,
                              @RequestParam(value = "size",defaultValue = "10") int size,
                              @RequestParam(value = "page",defaultValue = "1") int page,
                              @RequestParam(value = "sort",defaultValue = "created_time") String sort) {

        LambdaQueryWrapper<GoodEntity> queryWrapper = Wrappers.lambdaQuery(GoodEntity.class)
                .like(GoodEntity::getName,"%"+(search==null?"":search.trim())+"%")
                .orderByDesc(GoodEntity::getCreatedTime);

        return GridResult.of(goodService.page(new Page<>(page, size), queryWrapper),GoodResDTO::new);
    }




    //按照id查询
    @ApiOperation(
            value = "读取商品列表",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
            @ApiImplicitParam(name = "goo_id", value = "商品编号", required = true,dataType = "String",paramType = "path")
    })
    @GetMapping(value = "/demo/good/{goo_id}")
    public OperationResult<?> read(@PathVariable(value = "goo_id") String goo_id) {
        return OperationResult.ofSuccess(BeanCopyUtils.copyProperties(goodService.getById(goo_id),GoodResDTO::new));
    }






   //更新
    @ApiOperation(
            value = "更新商品记录",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
            //@ApiImplicitParam(paramType = "header", dataType = "String", name = "X-AUTH-TOKEN", value = "token", required = true)
    })
    @PutMapping(value = "/demo/good/{goo_id}")
    public OperationResult<?> update(@PathVariable("goo_id") String goo_id,
                                     @RequestBody @Validated  GoodUpdateRedDTO goodUpdateRedDto) {

        GoodEntity goodEntity = goodService.getById(goo_id);

        if (ObjectUtil.isEmpty(goodEntity)){
            return OperationResult.ofSuccess(null);
        }

        goodEntity.setName(goodUpdateRedDto.getName());
        goodEntity.setFactory(goodUpdateRedDto.getFactory());
        goodEntity.setNum(goodUpdateRedDto.getNum());

        goodService.updateById(goodEntity);

        return OperationResult.ofSuccess(BeanCopyUtils.copyProperties(goodEntity,GoodResDTO::new));
    }










   //删除
    @ApiOperation(
            value = "删除商品记录",
            produces = "application/json",
            consumes = "application/json",
            response = OperationResult.class
    )
    @ApiImplicitParams({
            //@ApiImplicitParam(paramType = "header", dataType = "String", name = "X-AUTH-TOKEN", value = "token", required = true)
    })
    @DeleteMapping(value = "/demo/good/{goo_id}")
    public OperationResult<?> delete(@PathVariable("goo_id") String goo_id) {
        GoodEntity goodEntity = goodService.getById(goo_id);
        goodService.removeById(goo_id);
        return OperationResult.ofSuccess(BeanCopyUtils.copyProperties(goodEntity, GoodResDTO::new));
    }


}
