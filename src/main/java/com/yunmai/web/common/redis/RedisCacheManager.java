package com.yunmai.web.common.redis;

import cn.hutool.core.util.BooleanUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

/**
 * @author WangChen
 * @since 2021-02-22 17:28
 **/
@Component
public class RedisCacheManager {

    private static final Logger logger = LoggerFactory.getLogger(RedisCacheManager.class);

    @Autowired
    private RedisTemplate<String, Object> redisTemplate;


    public boolean put(String key, Object value, long expire, TimeUnit timeUnit) {
        try {
            redisTemplate.opsForValue().set(key, value, expire, timeUnit);
        } catch (Exception ex) {
            logger.error("[RedisCacheManager put fail key = {}, value = {}]", key, value);
            return false;
        }
        return true;
    }

    public boolean delete(String key) {
        try {
            redisTemplate.delete(key);
        } catch (Exception ex) {
            logger.error("[RedisCacheManager delete fail key = {}]", key);
            return false;
        }
        return true;
    }


    public boolean put(String key, Object value) {
        try {
            redisTemplate.opsForValue().set(key, value);
        } catch (Exception ex) {
            logger.error("[RedisCacheManager put fail key = {}, value = {}]", key, value);
            return false;
        }
        return true;
    }


    public Object getValue(String key) {
        try {
            return redisTemplate.opsForValue().get(key);
        } catch (Exception ex) {
            logger.error("[RedisCacheManager getValue fail key = {}]", key);
            return null;
        }
    }

    public boolean hasKey(String key) {
        try {
            return BooleanUtil.isTrue(redisTemplate.hasKey(key));
        } catch (Exception ex) {
            logger.error("[RedisCacheManager getValue fail key = {}]", key);
            return false;
        }
    }


    public void addSet(String key, Object... objects) {
        try {
            redisTemplate.opsForSet().add(key, objects);
        } catch (Exception ex) {
            logger.error("[RedisCacheManager addSet fail key = {}]", key);
        }
    }

    public boolean existObj(String key, Object object) {
        try {
            return BooleanUtil.isTrue(redisTemplate.opsForSet().isMember(key, object));
        } catch (Exception ex) {
            logger.error("[RedisCacheManager existObj fail key = {}]", key);
            return false;
        }
    }

    public Set<Object> getSet(String key) {
        try {
            return redisTemplate.opsForSet().members(key);
        } catch (Exception ex) {
            logger.error("[RedisCacheManager addSet fail key = {}]", key);
        }
        return Collections.emptySet();
    }

    public void removeSetValue(String key, Object... objects) {
        try {
            redisTemplate.opsForSet().remove(key, objects);
        } catch (Exception ex) {
            logger.error("[RedisCacheManager removeSetValue fail key = {}]", key);
        }
    }

    public Set<Object> findUnionSets(List<String> groups) {
        try {
            return redisTemplate.opsForSet().union(groups);
        } catch (Exception ex) {
            logger.error("[RedisCacheManager findUnionSets fail key = {}]", groups.toString());
        }
        return Collections.emptySet();
    }


}
