package com.yunmai.web.common;

/**
 * @author WangChen
 * @since 2021-02-21 16:13
 **/
public interface Constant {

    /**
     * token请求头
     */
    String TOKEN = "X-AUTH-TOKEN";

    String TRACE_ID = "x-request-id";

    String IP = "ip";
}
