package com.yunmai.web.common.context;

import com.yunmai.account.center.sdk.vo.AccountGuestSession;

/**
 * @author WangChen
 * @since 2021-04-06 13:59
 **/
public class ContextHolder {

    private static final ThreadLocal<AccountGuestSession> HOLDER = new InheritableThreadLocal<>();

    public static void addSession(AccountGuestSession accountSession){
        HOLDER.set(accountSession);
    }

    public static AccountGuestSession getSession(){
        return HOLDER.get();
    }

    public static void remove(){
        HOLDER.remove();
    }
}
