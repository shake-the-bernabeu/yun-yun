package com.yunmai.web.utils;

import com.yunmai.framework.utils.mybatis.MyBatisCodeGenerator;

/**
 * @author WangChen
 * @since 2021-02-21 15:03
 **/
public class MybatisCode {

    public static void main(String[] args) {

        MyBatisCodeGenerator.builders()
                .dataSourceUrl("jdbc:mysql://localhost:3306/template_db?useUnicode=true&characterEncoding=UTF-8")
                .dataSourceDriver("com.mysql.cj.jdbc.Driver")
                .userName("root")
                .password("123456")
                .packageRootUrl("com.yunmai.web")
                .buildAndGenerate();
    }
}
