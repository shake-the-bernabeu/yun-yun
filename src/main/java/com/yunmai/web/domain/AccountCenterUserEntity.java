package com.yunmai.web.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.yunmai.framework.mybatis.entity.BaseEntity;
import io.swagger.annotations.ApiModel;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author WangChen
 * @since 2021-03-17
 */
@TableName("t_account_center_user")
@ApiModel(value="AccountCenterUserEntity对象", description="")
public class AccountCenterUserEntity extends BaseEntity<AccountCenterUserEntity> {

    private static final long serialVersionUID = 1L;

    private String authId;

    private String password;

    public String getAuthId() {
        return authId;
    }

    public void setAuthId(String authId) {
        this.authId = authId;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }

    @Override
    public String toString() {
        return "AccountCenterUserEntity{" +
            "authId=" + authId +
            ", password=" + password +
        "}";
    }
}
