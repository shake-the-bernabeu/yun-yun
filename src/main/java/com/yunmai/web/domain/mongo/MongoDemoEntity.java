package com.yunmai.web.domain.mongo;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.Date;

@Data
@Document(collection = "mongo_demo")
public class MongoDemoEntity {
    public static final String GUID_PREFIX = "mongo_demo";

    @Id
    @JsonProperty("_id")
    private String id;

    @JsonProperty("guid")
    private String guid;

    @JsonProperty("created_time")
    private Date created_time;

    @JsonProperty("updated_time")
    private Date updated_time;

    @JsonProperty("sort")
    private Long sort;

    @JsonProperty("version")
    private Long version;

    @JsonProperty("del")
    private Boolean del;

    @JsonProperty("name")
    private String name;

    @JsonProperty("age")
    private String age;

    @JsonProperty("address")
    private String address;
}