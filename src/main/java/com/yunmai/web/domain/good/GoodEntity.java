package com.yunmai.web.domain.good;


import com.baomidou.mybatisplus.annotation.TableName;
import com.yunmai.framework.mybatis.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

@Data
@AllArgsConstructor
@ToString
@TableName("t_good")
@NoArgsConstructor
public class GoodEntity extends BaseEntity<GoodEntity> {
    private static final long serialVersionUID = 1L;        //显示声明serialVersionUID可以避免对象不一致

    private String name;

    private Integer num;

    private String factory;

    @Override
    protected Serializable pkVal() {
        return null;
    }



}
