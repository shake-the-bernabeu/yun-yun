package com.yunmai.web.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.baomidou.mybatisplus.annotation.TableId;
import com.yunmai.framework.mybatis.entity.BaseEntity;
import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author WangChen
 * @since 2021-04-08
 */
@TableName("t_request_monitor")
public class RequestMonitorEntity extends BaseEntity<RequestMonitorEntity> {

    private static final long serialVersionUID = 1L;

    /**
     * 请求url
     */
    private String requestPath;

    /**
     * 毫秒
     */
    private Long millisecond;

    public String getRequestPath() {
        return requestPath;
    }

    public void setRequestPath(String requestPath) {
        this.requestPath = requestPath;
    }

    public Long getMillisecond() {
        return millisecond;
    }

    public void setMillisecond(Long millisecond) {
        this.millisecond = millisecond;
    }

    @Override
    public String toString() {
        return "RequestMonitorEntity{" +
                "requestPath='" + requestPath + '\'' +
                ", millisecond=" + millisecond +
                '}';
    }
}
