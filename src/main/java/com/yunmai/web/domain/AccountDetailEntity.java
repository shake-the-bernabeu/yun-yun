package com.yunmai.web.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.yunmai.framework.mybatis.entity.BaseEntity;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author WangChen
 * @since 2021-03-12
 */
@TableName("t_account_detail")
public class AccountDetailEntity extends BaseEntity<AccountDetailEntity> {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名
     */
    private String name;

    /**
     * 头像
     */
    private String avatar;

    /**
     * 账户id
     */
    private String accountId;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getAvatar() {
        return avatar;
    }

    public void setAvatar(String avatar) {
        this.avatar = avatar;
    }

    public String getAccountId() {
        return accountId;
    }

    public void setAccountId(String accountId) {
        this.accountId = accountId;
    }

    @Override
    public String toString() {
        return "AccountDetailEntity{" +
                "name='" + name + '\'' +
                ", avatar='" + avatar + '\'' +
                ", accountId='" + accountId + '\'' +
                '}';
    }
}
