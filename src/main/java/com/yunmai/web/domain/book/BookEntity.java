package com.yunmai.web.domain.book;

import com.yunmai.framework.mybatis.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.text.DecimalFormat;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BookEntity extends BaseEntity<BookEntity> {
    private String name;
    private DecimalFormat price;
    private String authors;
    private String isbn;
    private String publishingHouse;
    private Integer number;
    private String coverImageUrl;

}
