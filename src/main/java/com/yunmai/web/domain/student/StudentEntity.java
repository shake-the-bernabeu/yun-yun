package com.yunmai.web.domain.student;

import com.baomidou.mybatisplus.annotation.TableName;
import com.yunmai.framework.mybatis.entity.BaseEntity;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author WangChen
 * @since 2021-04-09
 */
@Data
@AllArgsConstructor
@ToString
@TableName("t_student")
@NoArgsConstructor
public class StudentEntity extends BaseEntity<StudentEntity> {

    private static final long serialVersionUID = 1L;        //显示声明serialVersionUID可以避免对象不一致

    private String name;

    private Integer age;

    private String address;

    @Override
    protected Serializable pkVal() {
        return null;
    }
}
