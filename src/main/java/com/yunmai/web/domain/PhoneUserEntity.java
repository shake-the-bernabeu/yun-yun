package com.yunmai.web.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.yunmai.framework.mybatis.entity.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

import java.io.Serializable;

/**
 * <p>
 * 
 * </p>
 *
 * @author WangChen
 * @since 2021-03-17
 */
@TableName("t_phone_user")
@ApiModel(value="PhoneUserEntity对象", description="")
public class PhoneUserEntity extends BaseEntity<PhoneUserEntity> {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty(value = "用户中心记录id")
    private String accountCenterUserId;

    @ApiModelProperty(value = "手机号")
    private String phone;

    @ApiModelProperty(value = "密码")
    private String password;

    public String getAccountCenterUserId() {
        return accountCenterUserId;
    }

    public void setAccountCenterUserId(String accountCenterUserId) {
        this.accountCenterUserId = accountCenterUserId;
    }
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    protected Serializable pkVal() {
        return null;
    }

    @Override
    public String toString() {
        return "PhoneUserEntity{" +
            "accountCenterUserId=" + accountCenterUserId +
            ", phone=" + phone +
            ", password=" + password +
        "}";
    }
}
