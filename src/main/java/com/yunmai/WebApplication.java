package com.yunmai;

import com.yunmai.framework.spring.boot.annotation.EnableYmFrameworkAutoConfiguration;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@ConfigurationPropertiesScan(basePackages = "com.yunmai.web.config.config")
@EnableConfigurationProperties
@MapperScan(basePackages = "com/yunmai/web/repositories")
@EnableYmFrameworkAutoConfiguration
@SpringBootApplication
public class WebApplication {

    public static void main(String[] args) {
        SpringApplication.run(WebApplication.class, args);
    }

}
