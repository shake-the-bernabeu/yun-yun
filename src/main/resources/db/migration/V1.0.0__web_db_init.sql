CREATE TABLE IF NOT EXISTS `t_account_center_user` (
  `guid` varchar(255) CHARACTER SET utf8 NOT NULL,
  `auth_id` varchar(255) NOT NULL,
  `password` varchar(255) DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `sort` tinyint(4) DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `is_del` tinyint(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `t_phone_user` (
  `guid` varchar(255) CHARACTER SET utf8 NOT NULL,
  `account_center_user_id` varchar(255) NOT NULL COMMENT '用户中心记录id',
  `phone` varchar(255) NOT NULL COMMENT '手机号',
  `password` varchar(255) NOT NULL COMMENT '密码',
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `sort` tinyint(4) DEFAULT NULL,
  `version` bigint(20) DEFAULT NULL,
  `is_del` tinyint(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;


CREATE TABLE IF NOT EXISTS `t_account_detail` (
  `guid` varchar(255) CHARACTER SET utf8 NOT NULL,
  `account_id` varchar(255) NOT NULL COMMENT '用户id',
  `name` varchar(255) NOT NULL COMMENT '用户名',
  `avatar` longtext NOT NULL COMMENT '头像',
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `sort` tinyint(4) DEFAULT '0',
  `version` bigint(20) DEFAULT NULL,
  `is_del` tinyint(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`guid`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 ROW_FORMAT=COMPACT;


CREATE TABLE IF NOT EXISTS `t_request_monitor` (
  `guid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `request_path` varchar(255) COLLATE utf8mb4_general_ci NOT NULL COMMENT '请求url',
  `millisecond` int(20) NOT NULL COMMENT '毫秒',
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `sort` tinyint(4) DEFAULT '0',
  `version` bigint(20) DEFAULT NULL,
  `is_del` tinyint(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;


CREATE TABLE IF NOT EXISTS `t_student` (
  `guid` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `age` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8mb4_general_ci DEFAULT NULL,
  `created_time` datetime DEFAULT NULL,
  `updated_time` datetime DEFAULT NULL,
  `sort` tinyint(4) DEFAULT '0',
  `version` bigint(20) DEFAULT NULL,
  `is_del` tinyint(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`guid`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;




