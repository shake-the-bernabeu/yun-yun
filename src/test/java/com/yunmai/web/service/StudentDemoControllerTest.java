package com.yunmai.web.service;

import com.yunmai.framework.utils.beancopy.BeanCopyUtils;
import com.yunmai.web.domain.student.StudentEntity;
import com.yunmai.web.services.IStudentService;
import com.yunmai.web.vo.student.StudentResDTO;
import org.junit.Assert;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.AutoConfigureOrder;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.Rollback;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.TestPropertySource;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.List;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.*;

@TestPropertySource("classpath:application-test.yaml")
@ActiveProfiles("test")
@Rollback(value = true)
@Transactional
@SpringBootTest
public class StudentDemoControllerTest {

    @Autowired
    IStudentService studentService;

    private StudentEntity getNewStudent()
    {
        String guid = "stud_id_" + UUID.randomUUID().toString().replace("-","");

        StudentResDTO studentResDTO = new StudentResDTO();
        studentResDTO.setAddress("测试地址");
        studentResDTO.setAge(30);
        studentResDTO.setModel();
        studentResDTO.setName("测试名称");
        studentResDTO.setGuid(guid);

        StudentEntity studentEntity = BeanCopyUtils.copyProperties(studentResDTO, StudentEntity::new);
        return studentEntity;
    }

    @Test
    void testCreate() {
        StudentEntity studentEntity = getNewStudent();
        boolean save = studentService.save(studentEntity);

        assertTrue(save,"创建学生失败");
    }

    @Test
    void testReadOne() {

        StudentEntity studentEntity = getNewStudent();
        boolean save = studentService.save(studentEntity);

        StudentEntity studentEntityCreated = studentService.getById(studentEntity.getGuid());
        Assert.assertEquals(studentEntity.getName(),studentEntityCreated.getName());
        Assert.assertEquals(studentEntity.getAddress(),studentEntityCreated.getAddress());
        Assert.assertEquals(studentEntity.getAge(),studentEntityCreated.getAge());
    }

    @Test
    void testReadList() {
        StudentEntity studentEntity1 = getNewStudent();
        StudentEntity studentEntity2 = getNewStudent();
        StudentEntity studentEntity3 = getNewStudent();
        List<StudentEntity> studentEntityListToCreate = Arrays.asList(studentEntity1,studentEntity2,studentEntity3);
        studentService.saveBatch(studentEntityListToCreate);

        List<String> guidList = Arrays.asList(studentEntity1.getGuid(),studentEntity2.getGuid(),studentEntity3.getGuid());
        List<StudentEntity> studentEntityList = studentService.list();
        assertNotNull(studentEntityList);
        //assertEquals(3,studentEntityList.size());
        for(StudentEntity studentEntityItem:studentEntityList)
        {
            assertTrue(guidList.contains(studentEntityItem.getGuid()));
        }
    }

    @Test
    void update() {
        String studentOldName = null;
        String studentNewName = null;

        StudentEntity studentEntity = getNewStudent();
        studentService.save(studentEntity);

        studentOldName = studentEntity.getName();
        studentNewName = studentOldName + "_新";

        studentEntity.setName(studentNewName);
        studentService.saveOrUpdate(studentEntity);

        StudentEntity studentEntityQuery = studentService.getById(studentEntity.getGuid());
        assertNotNull(studentEntityQuery);
        assertEquals(studentEntityQuery.getName(),studentNewName);
    }

    @Test
    void delete() {
        StudentEntity studentEntity = getNewStudent();
        studentService.save(studentEntity);

        StudentEntity studentEntityQuery = studentService.getById(studentEntity.getGuid());
        assertNotNull(studentEntityQuery);

        studentService.removeById(studentEntityQuery.getGuid());
        studentEntityQuery = studentService.getById(studentEntity.getGuid());
        assertNull(studentEntityQuery);

    }
    @Test
    void tess(){
        String url="http://lacalhost：8080/login";
        String substring = url.substring(url.lastIndexOf("/"));
        System.out.println(substring);
    }
}